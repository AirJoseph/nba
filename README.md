Please use your terminal:
to use the program please enter one of 3 options:
NBA_scrape.py all = scraping all data 
NBA_scrape.py <date> = scrapes data from specific date
NBA_scrape.py <start_date> <end_date> = scrapes data from range
the program prints 3 tables per game:
team vs. team stats
player stats
play by play data 